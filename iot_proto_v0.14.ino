/*
  response latency dropped to 2 seconds 
*/
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
const char* apiKey = "api_secret_key";
const char* deviceID = "deviceID";
const char* ssid     = "WiFi_name";
const char* password = "WiFi_password";
const char* relay1 = "switch_2";
const char* relay2 = "switch_1";

int c = 0;
int value1,value2;
WiFiClient client;
void setup() {
  value1=0;
  value2=0;
  Serial.begin(115200);
  delay(100);
 
  // We start by connecting to a WiFi network
 
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  pinMode(0, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, OUTPUT);
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void loop(){  
  // Use WiFiClient class to create TCP connections
  const int httpPort = 80;
  if (!client.connect("api.yeelink.net", httpPort)) {
    client.stop();
    Serial.println("connection failed");
    return ;
  }    
  else{
    getData();
  }
}
void getData(){
    client.print(String("GET ") + "/v1.0/device/"+ deviceID + "/sensors"+ " HTTP/1.1\r\n" +
               "Host: api.yeelink.net\r\n" + "U-ApiKey: " +apiKey +"\r\n"
               "Connection: close\r\n\r\n");
  delay(100);//change according to internet speed
  int i = 0;
  int value = 0;
  int idName;
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');//the json string in the 9th lines
    char data[110];
    StaticJsonBuffer<800> jsonBuffer;
    if(i==11){
      for(int k = 0;k<4;k++){
        for(int j=1;j<111;j++){
          data[j-1] = line[j+1+111*k];
        }
        JsonObject& root = jsonBuffer.parseObject(data);
        if (!root.success()){
             Serial.println("parseObject() failed");
            break;
          }
      else{
            idName = root["id"];
            value = root["last_data"];
            switch(idName){//faster implementation
               case 383073:
                Serial.print("value1:");
                Serial.println(value);
                if(value){
                   digitalWrite(5,HIGH);
                }
                else{
                   digitalWrite(5,LOW);
                }
                break;
               case 383076:
                Serial.print("value2:");
                Serial.println(value);
                if(value){
                   digitalWrite(4,HIGH);
                }
                else{
                  digitalWrite(4,LOW);
                }
                break;
               case 383079:
                Serial.print("value3:");
                Serial.println(value);
                if(value){
                   digitalWrite(0,HIGH);
                }
                else{
                  digitalWrite(0,LOW);
                }
                break;
             default:
               Serial.print("value4:");
                Serial.println(value);
                if(value){
                   digitalWrite(2,HIGH);
                }
                else{
                  digitalWrite(2,LOW);
                }
                break;
           }
        }
      }
    Serial.println("\n\n");
    }
    i++;
  }
}

